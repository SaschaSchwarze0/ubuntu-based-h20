FROM ubuntu

EXPOSE 54321

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y curl unzip openjdk-11-jre-headless

RUN apt-get install -y nginx
RUN apt-get install -y tinyproxy
RUN apt-get clean

RUN useradd -ms /bin/bash h2o

RUN cd /home/h2o && curl -O http://h2o-release.s3.amazonaws.com/h2o/rel-zermelo/1/h2o-3.32.0.1.zip
RUN cd /home/h2o && unzip h2o-3.32.0.1.zip

ADD tinyproxy.conf /etc/tinyproxy/tinyproxy.conf

WORKDIR /home/h2o/
ADD start.sh /home/h2o/start.sh
RUN chmod +x /home/h2o/start.sh

CMD ["./start.sh"]